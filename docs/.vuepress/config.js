module.exports = {
	title: 'Anix',
	dest: 'public',
	base: '/anix-docs/',
	plugins: [
			['@vuepress/back-to-top', true],
	],
	themeConfig: {
		displayAllHeaders: true,
		nav: [
			{ text: 'Home', link: '/' },
			{ text: 'Guide', link: '/guide/' },
			{ text: 'Gitlab', link: 'https://framaclic.org/h/anix-gitlab' },
		],
		sidebar: [
			{
				title: 'First steps',
				collapsable: false,
				sidebarDepth: 1,
				children: [
					'/guide/first-steps/getting-started',
					'/guide/first-steps/virtual-machine',
					'/guide/first-steps/real-hardware',
					'/guide/first-steps/install-packages',
				],
			},
			{
						title: 'Advanced',
						collapsable: false,
						sidebarDepth: 1,
						children: [
							'/guide/advanced/for-programmers',
							'/guide/advanced/contribute',
						],
				}
		],
    repo: 'https://gitlab.com/anix-project/anix',
    repoLabel: 'Contribute!',

    docsRepo: 'https://gitlab.com/anix-project/anix-docs',
    docsDir: 'docs',
    editLinks: true,
    nextLinks: true,
    prevLinks: true,
    editLinkText: 'Help us improve this page!',
	}
}
