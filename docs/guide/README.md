---
prev: false
next: /guide/first-steps/getting-started
---

# Introduction
Anix is a multitasking and monolithic Operating System (OS) under GPL license written in Rust.  
But, what this sentence means?

| Word   | Description |
| ------ | ----------- |
| Anix   | It is the name of the OS :wink:. |
| Multitasking | [A multitasking OS](https://en.wikipedia.org/wiki/Computer_multitasking) is an OS which can run several tasks. |
| Monolithic    | [A monolithic OS](https://en.wikipedia.org/wiki/Monolithic_system) is an OS which load all of its drivers in the kernel mode, so it is not good for security. Anix is intended to someday become a [microkernel](https://en.wikipedia.org/wiki/Microkernel) (the opposite of a monolithic OS).|
| OS | An [OS](https://en.wikipedia.org/wiki/Operating_system) is a program that manages computer hardware.|
| GPL | [GPL](https://www.gnu.org/licenses/gpl-3.0.html) is a free software license. So, you can read, modify and redistribute the source code.|
| Rust | [Rust](https://www.rust-lang.org/) is a programming language which is fast, safe and it is a system programming language.|
Now, you know what this sentence means. You can [try
Anix](/guide/first-steps/getting-started).
