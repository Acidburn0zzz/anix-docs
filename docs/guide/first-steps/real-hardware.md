---
prev: /guide/first-steps/virtual-machine
next: /guide/first-steps/install-packages
---

# Try Anix on real hardware
First, install all needed packages [here](/guide/first-steps/install-packages).
Then, type `make` and Anix will be compiled, linked and copied on your disk.
Finally, insert your disk (it must be a SATA drive) in your computer and power
on it.
