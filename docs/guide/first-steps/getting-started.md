---
prev: /guide/
next: /guide/first-steps/virtual-machine
---

# Getting started
You can try Anix:
  * [On a virtual machine](/guide/first-steps/virtual-machine)
  * [On real hardware](/guide/first-steps/real-hardware)
