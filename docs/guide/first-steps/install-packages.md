---
prev: /guide/first-steps/real-hardware
next: /guide/advanced/for-programmers
---

# Install all needed packages
First, download the code [here](https://gitlab.com/anix-project/anix).  
Then, install the following packets (because I am on a GNU/Linux system, I will use
the command line to install the packets but I do not known how to do this on
Windows):
  * Install Qemu (the virtual machine software): `sudo apt-get install qemu
    qemu-kvm`.
  * Install Nasm (an assembly assembler): `sudo apt-get install nasm`.
  * Install Rustup (a toolchain manager of Rust): see [this page](https://rustup.rs).
  * Install Xargo (a cross-compiler for Rust): `cargo install xargo`.
  * Install Xorriso (an ISO creator): `sudo apt-get install xorriso` (if you
    want to try Anix on real hardware, it is not necessary).
  * Install lld (the Rust linker), keep going, it is the more complicated step,
    if you are not on Debian, you may need to change the
    Makefile: open the Makefile with your prefered editor, search the line with
    ld.lld and replace it by the packet name. To install it:
    * On Debian: `sudo apt-get install lld`.
    * On Ubuntu 16.04: `sudo apt-get install lld-6.0`.
    * On Ubuntu 18.04: `sudo apt-get install lld-8.0`.
    * For another GNU/Linux distribution: search `ld.lld your-distribution-name`
  * With Rustup, install the nightly toolchain: `rustup toolchain install nightly`.
  * With Rustup, install the rust-src component: `rustup component add rust-src`.

Phew! You had installed all required packages! Now, just set the Rust nightly toolchain
as default in this directory: `rustup override set nightly` in the Anix root.  
