---
prev: /guide/first-steps/getting-started
next: /guide/first-steps/real-hardware
---

# Try Anix on a virtual machine
To try Anix without having another computer, you can use a virtual machine. It is
a program which simulates a computer. There are a lot of options such as:
  * [Qemu](https://qemu.org) (licensed under GPLv2 with third-party code under
    licenses compatible with the GPLv2 license, see [here](https://git.qemu.org/?p=qemu.git;a=blob;f=LICENSE;h=9389ba614f80b232ffb99d4121983f63ac86d845;hb=HEAD)).
  * [Bochs](http://bochs.sourceforge.net/) (licensed under LGPL with third-party
    code under licenses which are compatible with the LGPL license, see [here](https://sourceforge.net/p/bochs/code/HEAD/tree/trunk/bochs/LICENSE)).
  * [VirtualBox](https://www.virtualbox.org/) (licensed under GPLv2 but with
    third-party code under licenses which the Free Software Foundation considers
    incompatible with the GPL, see [here](https://www.virtualbox.org/browser/vbox/trunk/COPYING)).
  * [Virt-manager](https://virt-manager.org/) (licensed under GPLv2, see
    [here](https://github.com/virt-manager/virt-manager/blob/master/COPYING)),
    it uses Qemu as backend.

For now, Anix works perfectly with qemu and virt-manager but was not
tested with VirtualBox and Bochs. In [this section](/guide/first-steps/install-packages), I will guide you to try Anix on Qemu.
Finally, run `make qemu` and Anix will be compiled, linked and executed.
