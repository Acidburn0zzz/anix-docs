---
prev: /guide/advanced/for-programmers
next: false
---

# How to contribute
To contribute, look at the Anix repository
([here](https://gitlab.com/anix-project/anix)).
You need to know the Rust programming language.
