---
prev: /guide/first-steps/install-packages
next: /guide/advanced/contribute
---

# For programmers
[How to contribute](/guide/advanced/contribute)
