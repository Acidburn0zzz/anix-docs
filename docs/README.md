---
home: true
heroImage: /assets/logo.svg
heroText: Anix
tagline: Anix is a multitasking and monolithic Operating System (OS) under GPL license written in Rust.
actionText: Get Started →
actionLink: /guide/
features:
- title: "Fully compatible"
  details: "Anix is fully compatible with GNU/Linux and with POSIX too"
- title: "Everyone has its own distribution"
  details: "Anix will have a lot of distributions for each type of person (gamer,
  developper, teacher, noob)"
- title: "Safe"
  details: "Anix is entirely written in Rust which is a very safe language"
footer: GPLv3 Licensed | Copyright © 2018-2019 Nifou
---

